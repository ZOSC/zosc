import Home from "./home/home.vue";
import FAQ from "./faq/faq.vue";

export default vue => [
	{
		path: "",
		controller: () => {
			vue.currentView = Home;
		}
	},
	{
		path: "faq",
		controller: () => {
			vue.currentView = FAQ;
		}
	}
];